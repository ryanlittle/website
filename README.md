# Personal homepage

Website template designed by [Fábio Madeira](https://github.com/biomadeira/biomadeira.github.io).

Powered by [Jekyll](http://jekyllrb.com/).

Website software under the MIT License (MIT). See [LICENSE](LICENSE.md)
